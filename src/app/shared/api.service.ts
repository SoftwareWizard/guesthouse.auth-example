import {
  HttpClient,
  HttpErrorResponse,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class ApiService {
  constructor(private http: HttpClient) {}

  getVersionInfo(): Observable<string> {
    return this.http
      .get<any>('https://localhost:9001/info', {
        responseType: 'text' as 'json',
      })
      .pipe(
        catchError((e: HttpErrorResponse) =>
          of(`🌩 API Error: ${e.status} ${e.statusText}`)
        )
      );
  }

  getEmployees(): Observable<any> {
    return this.http.get<any>('https://localhost:9001/api/v1/Employees').pipe(
      map(
        (response: any) =>
          `☁ API Success : ${response.data[0].firstName} ${response.data[0].lastName} (${response.data[0].employeeTitle})`
      ),
      catchError((e: HttpErrorResponse) =>
        of(`🌩 API Error: ${e.status} ${e.statusText}`)
      )
    );
  }
}
