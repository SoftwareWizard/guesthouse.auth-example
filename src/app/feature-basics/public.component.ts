import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'app-public',
  template: `
    <div class="alert alert-success">
      This is the <strong>🌐 PUBLIC</strong> component.
    </div>
    <div class="alert alert-info">
      <h5>guesthouse.service-example Info</h5>
      <strong>{{ versionInfo$ | async }}</strong>
    </div>
  `,
})
export class PublicComponent implements OnInit {
  public versionInfo$!: Observable<string>;

  constructor(private apiService: ApiService) {}

  async ngOnInit() {
    this.versionInfo$ = this.apiService.getVersionInfo();
  }
}
